moltty
======

macOS Terminal.app profile -> iTerm2 colors converter

A Swift port of [Plumage](https://github.com/kourge/plumage).

How to Use
----------

`swift ./src/Moltty.swift < PROFILE.terminal > PROFILE.itermcolors`

If the resulting colors do not appear to match, increase the **Minimum
Contrast** (Preferences -> Colors). This discrepancy may be due to a
Terminal.app setting or fixed behavior.

**Note:** The resulting `.itermcolors` file currently encodes color component
values as `<string>` (not `<real>`), but this has no discernible effect on the
color display.

Limitations
-----------

The following color groups must be manually adjusted (as there are no
equivalents in Terminal.app):

* Links
* Selected Text
* Badge
* Tab Color
* Underline Color
* Cursor Guide
