// Port of https://github.com/kourge/plumage

import Foundation
import AppKit

let TO_ITERM_DICT = [
      "ANSIBlackColor":         "Ansi 0 Color",
      "ANSIRedColor":           "Ansi 1 Color",
      "ANSIGreenColor":         "Ansi 2 Color",
      "ANSIYellowColor":        "Ansi 3 Color",
      "ANSIBlueColor":          "Ansi 4 Color",
      "ANSIMagentaColor":       "Ansi 5 Color",
      "ANSICyanColor":          "Ansi 6 Color",
      "ANSIWhiteColor":         "Ansi 7 Color",

      "ANSIBrightBlackColor":   "Ansi 8 Color",
      "ANSIBrightRedColor":     "Ansi 9 Color",
      "ANSIBrightGreenColor":   "Ansi 10 Color",
      "ANSIBrightYellowColor":  "Ansi 11 Color",
      "ANSIBrightBlueColor":    "Ansi 12 Color",
      "ANSIBrightMagentaColor": "Ansi 13 Color",
      "ANSIBrightCyanColor":    "Ansi 14 Color",
      "ANSIBrightWhiteColor":   "Ansi 15 Color",

      "BackgroundColor":        "Background Color",
      "TextBoldColor":          "Bold Color",
      "CursorColor":            "Cursor Color",
      "TextColor":              "Foreground Color",
      "SelectionColor":         "Selection Color"
]

enum ConversionError: Error {
  case plistParseError
  case colorParseError(_ color: String)
}

func terminalToIterm(_ input: String) throws -> String? {
  let plist = input.propertyList() as? NSDictionary

  if plist == nil {
    throw ConversionError.plistParseError
  }

  var itermDict = [String: [String: String]]()

  for (terminalKey, itermKey) in TO_ITERM_DICT {
    do {
      if let data = plist![terminalKey] as? Data,
        let color = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data)
          as? NSColor {
        itermDict[itermKey] = [
          "Alpha Component": color.alphaComponent.description,
          "Blue Component": color.blueComponent.description,
          /* "Color Space": color.colorSpace.localizedName ?? "sRGB", */
          "Color Space": "sRGB",
          "Green Component": color.greenComponent.description,
          "Red Component": color.redComponent.description,
        ]
      } else {
        throw ConversionError.colorParseError(terminalKey)
      }
    } catch {
      throw ConversionError.colorParseError(terminalKey)
    }
  }

  let encoder = PropertyListEncoder.init()
  encoder.outputFormat = PropertyListSerialization.PropertyListFormat.xml
  let data = try encoder.encode(itermDict)

  return String(data: data, encoding: String.Encoding.ascii)
}

func main() {
  var input = ""

  while let line = readLine(strippingNewline: false) {
    input += line
  }

  do {
    if let converted = try terminalToIterm(input) {
      print(converted)
    } else {
      fputs("Failed to convert\n", stderr)
    }
  } catch ConversionError.plistParseError {
    fputs("Error parsing input\n", stderr)
  } catch ConversionError.colorParseError(let key) {
    fputs("Error parsing color \(key)\n", stderr)
  } catch {
    fputs("\(error)\n", stderr)
  }
}

main()
